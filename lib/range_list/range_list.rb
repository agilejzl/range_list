# A range list is an aggregate of ranges, for example: [1, 5), [10, 11), [100, 201)
class RangeList
  def initialize(data = 0)
    @data = data
  end

  def add(range = [])
    bitint = range_to_bitmap(range)
    @data |= bitint
  end

  def remove(range = [])
    bitint = range_to_bitmap(range)
    @data = (@data | bitint) - bitint
  end

  # for exmaple: range [1, 5) converted to 0b11110
  def range_to_bitmap(range = [])
    bitnum = 0b0
    bit1_start = range[0]
    bit1_end = range[1] - 1
    return bitnum unless bit1_end >= bit1_start

    bitnum |= (1 << (bit1_end - bit1_start + 1)) - 1
    bitnum << bit1_start
  end

  def print
    ranges_arr = []
    tmp_data = @data
    bit_index = 0
    while tmp_data > 0
      if bit_index == 0
        bitstr = tmp_data.to_s(2)
        bit_size = bitstr.size
      end
      curr1_start = bitstr.index('1')
      curr1_end = bitstr.index('0') - 1

      if curr1_start >= 0 && !curr1_end.nil?
        curr_range = [bit_size - curr1_end - 1, bit_size - curr1_start]
        ranges_arr.push(curr_range)
        tmp_data -= range_to_bitmap(curr_range)
        bit_index = 0
        next
      end
      bit_index += 1
    end
    ranges_str = ranges_arr.reverse.map.with_index {|arr, i| "[#{arr[0]}, #{arr[1]})" }.join(' ')
    puts ranges_str
    ranges_str
  end
end
