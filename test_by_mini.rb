
require 'byebug'
require 'minitest/autorun'
require './lib/range_list/range_list.rb'

describe RangeList do
  before do
    @rl = RangeList.new
    @time_start = Time.now.to_f
  end

  describe "RangeList test cases" do
    it "RangeList add/remove" do
      @rl.add([1, 5])
      assert_equal '[1, 5)', @rl.print

      @rl.add([10, 20])
      assert_equal '[1, 5) [10, 20)', @rl.print

      @rl.add([20, 20])
      assert_equal '[1, 5) [10, 20)', @rl.print

      @rl.add([20, 21])
      assert_equal '[1, 5) [10, 21)', @rl.print

      @rl.add([2, 4])
      assert_equal '[1, 5) [10, 21)', @rl.print

      @rl.add([3, 8])
      assert_equal '[1, 8) [10, 21)', @rl.print

      @rl.remove([10, 10])
      assert_equal '[1, 8) [10, 21)', @rl.print

      @rl.remove([10, 11])
      assert_equal '[1, 8) [11, 21)', @rl.print

      @rl.remove([15, 17])
      assert_equal '[1, 8) [11, 15) [17, 21)', @rl.print

      @rl.remove([3, 19])
      assert_equal '[1, 3) [19, 21)', @rl.print

      base = 2**25
      @rl.add([base-2, base])
      print_spent("add #{base}")
      assert_equal "[1, 3) [19, 21) [#{base-2}, #{base})", @rl.print
      print_spent("print #{base}")

      @rl.remove([base-2, base])
      print_spent("remove #{base}")
      assert_equal "[1, 3) [19, 21)", @rl.print
      print_spent("print #{base}")
    end
  end

  def print_spent(action)
    time_second = (Time.now.to_f - @time_start).round(3)
    puts "Action #{action}, Spent time #{time_second}S"
    @time_start = Time.now.to_f
  end
end
